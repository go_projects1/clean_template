package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"log"
	"os"
)

type Config struct {
	HTTPPort         string
}

// Load ...
func Load() *Config {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
	config := &Config{HTTPPort:         cast.ToString(env("HTTP_PORT", "8384")),}
	return config
}

func env(key string, defaultValue interface{}) interface{} {
	if val, exists := os.LookupEnv(key); exists {
		return val
	}

	return defaultValue
}