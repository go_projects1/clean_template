run:
	go run cmd/main.go

gitcommit:
	git add .
	git commit -m 'Changes'

gitpushbank:
	git push -u origin master

gitpushcloud:
	git push -u gitlabcom

tidy-vendor:
	go mod tidy && go mod vendor

migrate-up:
	migrate -path=./migrations -database=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${POSTGRES_DB}?sslmode=disable up

migrate-down:
	migrate -path=./migrations -database=postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${POSTGRES_DB}?sslmode=disable down

swag-init:
	swag init -g cmd/main.go -o docs