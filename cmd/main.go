package main

import (
	"context"
	"fmt"
	"hamkor-mobile/clean_template/bootstrap"
	"hamkor-mobile/clean_template/config"
	"os"
	"os/signal"
)

func main()  {
	fmt.Println("Application starting....")
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)

	cfg := config.Load()

	app := bootstrap.New(cfg)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		fmt.Println(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()
	app.Run(ctx)

}
