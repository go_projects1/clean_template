package rest

import (
	"net/http"
	"github.com/gin-gonic/gin"
)

type Server struct {
	router *gin.Engine
}

func New() *Server {
	server := &Server{}
	server.router = gin.Default()
	server.endpoints()
	return server
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}
func (s *Server) endpoints() {
	bff := s.router.Group("/pay")
	bff.Use()
	{
		bff.GET("/ping",getPing)
	}
}

func getPing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "welcome",
	})
}
