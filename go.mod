module hamkor-mobile/clean_template

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.4.1
)
