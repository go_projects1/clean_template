package bootstrap

import (
	"context"
	"errors"
	"fmt"
	"hamkor-mobile/clean_template/config"
	"hamkor-mobile/clean_template/ports/rest"
	"log"
	"net/http"
	"time"
)

type App struct {
	teardown   []func()
	//	http server
	http *http.Server
	//	main config
	config *config.Config
}

func New(cfg *config.Config) *App {
	app := App{}
	teardown := make([]func(), 0)

	teardown = append(teardown, func() {
		fmt.Println("Goodbye...")
	})

	app.teardown = teardown
	app.config = cfg
	restServer := rest.New()

	app.http = &http.Server{
		Addr:         cfg.HTTPPort,
		Handler:      restServer,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  5 * time.Minute,
	}

	return &app
}

func (app *App) Run(ctx context.Context) {
	go func(c context.Context) {

	}(ctx)

	go func() {
		log.Println("REST Server started at port " + app.config.HTTPPort)
		if err := app.http.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal(fmt.Sprintf("Failed To Run REST Server: %s\n", err.Error()))
		}

		log.Println("http server has been shut down")
	}()
	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}
	ctxShutDown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err := app.http.Shutdown(ctxShutDown); err != nil {
		log.Fatal(fmt.Sprintf("http server shutdown failed:%s\n", err.Error()))
	}
}