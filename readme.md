## Clean architecture for golang microservice


```bash

├── bootstrap
│   └── app.go
├── cmd
│   └── main.go
├── config
│   └── config.go
├── docs
│   └── swagger.yaml
├── controllers
│   └── controller.go
├── adapters
│   ├── services
│   └── storage
│       ├── postgre
│       ├── redis
│       └── store.go 
├── domains
├── errors
├── ports
│   ├── rest
│       └── middleware.go
│   ├── grpc
│       └── server.go
│   ├── mq
│       └── pool.go
│   └── schedulers
├── migrations
├── utils
│   └── commons.go
├── usecases
│   └── usecase.go
├── Dockerfile
├── Makefile
├── .env
├── .gitignore
└── readme.md
```
